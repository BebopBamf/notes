:PROPERTIES:
:ID:       db57027f-7103-4bde-a810-946f6fb8d97c
:END:
#+title: Hilbert's Nullstellensatz (Computational Problem)
#+author: Euan Mendoza

The Hilbert's Nullstellensatz Problem (HN) is the computational problem of deciding if Hilbert's Nullstellensatz is satisfiable over $\C$.

* TODO Blum-Shub-Shmale NP algorithm

* TODO AM protocol for HN

A interactive proof protocol was introduced by Koiran[cite:@Koiran1996]
