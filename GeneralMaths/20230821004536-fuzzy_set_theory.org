:PROPERTIES:
:ID:       b95138f8-0a74-4f9e-ad42-a0046f0912f6
:END:
#+title: Fuzzy Set Theory

Fuzzy set theory can build analogues of classical set theory, see [[id:b0a42749-a436-4901-9cde-8ae21a017d5c][Set Theoretic Structures]]

* Zadeh Fuzzy Sets
** Two methods of set theory

There are two ways of understanding sets.

As a collection of 'things'.

\begin{align*}
A = \{ a_{0}, a_{1}, \ldots, a_{n}\}
\end{align*}

Or defined by some predicate condition.

\begin{align*}
A = \{ x \mid P(x) \}
\end{align*}

where $P(x)$ is a predicate that determines whether x is in the set or not.

The *characteristic function* can be defined as follows.
