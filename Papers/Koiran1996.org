:PROPERTIES:
:ID:       bfa02cb7-6d4f-45ca-9c0e-79db0e972656
:ROAM_REFS: @Koiran1996
:END:
#+title: Koiran, Pascal :: Hilbert’s Nullstellensatz is in the Polynomial Hierarchy.

This paper places [[id:db57027f-7103-4bde-a810-946f6fb8d97c][Hilbert's Nullstellensatz (Computational Problem)]] in the polynomial time hierarchy.
