#+title: Karate Competition Warm Up Guidelines
#+author: Euan Mendoza
#+date: June, 2024

* TODO Attitude

 * Pay attention to your body and your emotions.

* TODO Arriving to the Competition

 * Be early, comps never run on time.
 * Know where the BBMA squad is in case you need to ask a question.

* TODO Advice for Competition Day

** Don't Overthink

 * It is unlikely that you will be able to learn anything new 2 hours from the competition, stick to what you already know and have practiced.
 * Don't overthink the warm-up. It is just a warm-up, it is not a full sparring/kata lesson.
 * Trust and back yourself. Don't worry about small little details, just focus on one thing at a time.

** Attitude

 * Being nice is very important, there is always going to be a winner and a loser and even if the person isn't on your team or you don't know them, it is not an excuse to make them feel terrible.
 * While Karate Comps can be very fun, for some people they are also very stressful and are a big commitment. It is no small feat to train for something for months, that only lasts 3 minutes. For this reason being mean even in a teasing way is not ok. Leave the banter for the day after the competition.
 * Be proud of the effort you put in, even if you don't win gold, think about what you did well and what you did not so well. Very helpful for Kata, even if you don't win you can walk away proud of a solid effort.

* General Advice for Warming Up

 * Do competition specific warm ups. Push ups, jogging is not so helpful to a kata competition. Better to practice the actual kata.
 * Bring a jumper, you warm down easily as well.
 * For competition, a warm up is also about the mind and spirit. Getting a feel for what it looks like to do a move with maximum effort, as well as getting rid of nerves.

* TODO Kata

** Warm up

 * The Kata moves are stretches, practicing the kata stances slowly is a very good idea. Getting a feel of what maximum depth feels like.
 * Alternating between doing the moves fast, and having a break by going slow is the best way to warm up. It is how the competitors recommend warming up.
 * Focus on the signature parts of the Kata, Heian Yondan the kicks for example, make sure you can do the highlights of the Kata both fast and well.
 * For my warm up i'll go through the moves slowly first to get a feel for the pattern, than i'll go fast as possible. Than i'll choose a section of the Kata and focus on one aspect, like a solid Kick, or a good snap sound on a punch. Than i'll repeat the process.
 * Never ever go at medium speed, mentally you should always go at 100 percent effort whether 100 percent correct slow moves, or 100 percent physicality. A slow technique does not have loose posture, it still snaps and it still gives off a vibe that you don't want to stand in the road of the punch even though it's slow. You can see examples of this

** Things to think about

 * What do you do well that you can highlight?
 * Don't overthink about things you are bad at, just get them to a place where it is sound, and focus on the things you are good at.
 *

* TODO Kumite
