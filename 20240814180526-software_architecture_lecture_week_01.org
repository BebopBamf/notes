:PROPERTIES:
:ID:       203966f0-b4bc-442a-b0f5-41087b1cf356
:END:
#+title: Software Architecture Lecture - Week 01
#+author: Euan Mendoza
#+date: <Mon 2024-

 * [[id:14c27f01-75e4-4c71-821d-50dceb028d99][Software Architecture Definition]]
 * [[id:958cb53d-4cac-4509-95f6-04cf2adbdf7a][Software Architecture Structures and Views]]
