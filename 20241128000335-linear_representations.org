:PROPERTIES:
:ID:       be4c9ef8-81bf-4f8d-919d-5d5a624d2a7d
:END:
#+title: Linear Representations
#+author: Euan Mendoza
#+filetags: :representation-theory:

Given a finite group $G$, a /linear representation of/ $G$ is a [[id:78b33056-9933-4c3d-b127-1b92c7aac28f][group homomorphism]] $\rho : G\to \GL(n,\C)$ from a finite group to the [[id:79087b0a-8fcc-4b98-8a8b-6aa674999807][general linear group]] over complex numbers.

#+attr_latex: :options {Shur's Lemma}
#+begin_lemma
Given representations
#+end_lemma
