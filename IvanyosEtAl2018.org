:PROPERTIES:
:ID:       1285647a-b066-424e-b8c8-5b39393c040c
:ROAM_REFS: @IvanyosEtAl2018
:END:
#+title: Ivanyos, Gábor and Qiao, Youming and Subrahmanyam, K. V. :: Constructive Non-Commutative Rank Computation Is in Deterministic Polynomial Time.
